({
    doInit: function(component, event, helper) {
        var test ='';
    },
    
    validate:function(component, event, helper){
         return component.find('form').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
             console.log(inputCmp.get('v.validity').valid);
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
    },

    
    remove:function(component, event, helper){
        // needs to be deleted on the server
        component.set('v.item.IsDeleted', true);
        
        var item = component.get('v.item');
        
      
        var createEvent = component.getEvent("item:delete");
        createEvent.setParams({ "item": item});
        createEvent.fire();
    }
})