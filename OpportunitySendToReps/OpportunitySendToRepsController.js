({
    doInit: function(component, event, helper) {
       helper.getCount(component);
       helper.getRepNames(component);
   },
   
   handleShowModal : function(component, event, helper) {
       component.set('v.itemsLoading', true);
       helper.getItems(component);
       var cmpTarget = component.find('Modalbox');
       var cmpBack = component.find('Modalbackdrop');
       $A.util.addClass(cmpTarget, 'slds-fade-in-open');
       $A.util.addClass(cmpBack, 'slds-backdrop--open');
   },
   
   handleCloseModal: function(component, event, helper){
       helper.closeModal(component);
   },
   
   addRow: function(component, event, helper){
       var opportunityProcess = component.get('v.opportunityProcess');
       
       opportunityProcess.push({
           sobjectType: 'Opportunity_Process__c',
           Opportunity__c: component.get('v.recordId'),
           Buy_price__c: false,
           Unit_Rates__c: false,
           Received_From_Reb__c:false,
           Received_Reps_Proposal__c: '',
           Rep_Name__c:'',
           Sent_To_Reps__c:''
       });
       
       component.set('v.opportunityProcess', opportunityProcess);
   },
   
   handleDelete:function(component, event, helper){
       component.set('v.opportunityProcess', component.get('v.opportunityProcess'));
   },
   
   save:function(component, event, helper){
       
        var validForms= [].concat(component.find('itemForm') || []).reduce(function (validSoFar, itemFormCmp) {
           return itemFormCmp.validate() && validSoFar ;
       }, true);
       
       if(! validForms) return;
       
       
       var items = component.get('v.opportunityProcess');
       var itemsToUpdate = items.filter(function(item){
           return item.Id || (! item.Id && ! item.IsDeleted);
       });
       
       var saveItems = component.get("c.saveItems");
       saveItems.setParams({
           "items": itemsToUpdate
       });
       

       saveItems.setCallback(this, function(response) {
           var state = response.getState();
           if (state === "SUCCESS") {
               helper.getCount(component);
               helper.closeModal(component);
           }
           else {
               console.log("Failed with state: " + state);
           }
       });
       $A.enqueueAction(saveItems);
       component.set('v.itemsLoading', true); 
       
   }
   
})