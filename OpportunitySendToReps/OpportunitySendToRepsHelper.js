({
	 getItems: function(component, recordId){
        var action = component.get("c.getItems");
        action.setParams({
            "opportunityId": component.get("v.recordId")
        });
        
 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var val = response.getReturnValue()
                component.set("v.opportunityProcess", response.getReturnValue());
                component.set("v.opportunityProcessCount", val.length);
                component.set('v.itemsLoading', false);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
   
        $A.enqueueAction(action);
    },
    getCount:function(component){
        var getCount = component.get("c.getCount");
        getCount.setParams({
            "opportunityId": component.get("v.recordId")
        });
        
 
        getCount.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.opportunityProcessCount", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(getCount);
    },
    
    getRepNames:function(component){
      var getRepNames = component.get("c.getRepNames");

        getRepNames.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.repNames", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        
        $A.enqueueAction(getRepNames)  
    },
    
    closeModal:function(component){
        var cmpTarget = component.find('Modalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
        
        component.set('v.itemsLoading', false);
        component.set('v.opportunityProcess', []);
    }
})