public class OpportunityProcessController {
    
    public static String recordTypeId = '0121N000001THzAQAW';
   
    
    
	@AuraEnabled
   	public static List<Opportunity_Process__c> getItems(Id opportunityId) {

        Opportunity_Process__c test = new Opportunity_Process__c();
       
		return [SELECT 
                name,
                Opportunity__c,
                Buy_price__c,
                Unit_Rates__c,
                Received_From_Reb__c,
                Received_Reps_Proposal__c,
                Rep_Name__c,
                Sent_To_Reps__c
                from Opportunity_Process__c 
                WHERE Opportunity__c=:opportunityId and recordTypeId=:recordTypeId ];
  	}
        
    @AuraEnabled
    public static Integer getCount(Id opportunityId) {
    	return [SELECT count() from Opportunity_Process__c WHERE Opportunity__c=:opportunityId and recordTypeId=:recordTypeId];
    }
    
    @AuraEnabled
    public static List<SelectOption> getRepNames()
	{
      List<SelectOption> options = new List<SelectOption>();
        
   		Schema.DescribeFieldResult fieldResult = Opportunity_Process__c.Rep_Name__c.getDescribe();
   		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
      	for( Schema.PicklistEntry f : ple)
       	{
         	 options.add(new SelectOption(f.getLabel(), f.getValue()));
       	}     
       	return options;
	}
    
	@AuraEnabled
    public static Boolean saveItems(List<Opportunity_Process__c> items){
        
        List<Opportunity_Process__c> toInsert = new List<Opportunity_Process__c>();
        List<Opportunity_Process__c> toUpdate = new List<Opportunity_Process__c>();
        List<Opportunity_Process__c> toDelete = new List<Opportunity_Process__c>();
        
        for(Opportunity_Process__c item  : items){
            
            if(String.isNotEmpty(item.Id) && item.IsDeleted){
                toDelete.add(item);
                System.debug('toDelete');
                continue;
            }
            
            if(String.isNotEmpty(item.Id)){
                toUpdate.add(item);
                System.debug('toUpdate');
                continue;
            }
            
            item.recordTypeId = recordTypeId;
            toInsert.add(item);
            System.debug('toInsert');
           
     	}
        
        Savepoint sp = Database.setSavepoint();
        
        try {
    		Database.delete(toDelete, true);
            Database.update(toUpdate, true);
            Database.insert(toInsert, true);
            return true;
	
		} catch (DmlException e) {
            Database.rollback(sp);
    		return false;
		}

    }    
    
}